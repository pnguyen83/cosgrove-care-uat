trigger skedJobAllocationTrigger on sked__Job_Allocation__c (after insert, after update) {
	skedJobAllocationHandler handler = new skedJobAllocationHandler();

	if (trigger.isAfter) {
		if (trigger.isInsert) {
			handler.afterInsert(trigger.new);
		}
		else if (trigger.isUpdate) {
			handler.afterUpdate(trigger.new, trigger.oldMap);
		}
	}
	
}