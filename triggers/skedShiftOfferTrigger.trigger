trigger skedShiftOfferTrigger on Shift_Offer__c (before update, after update) {
    if ( Trigger.isBefore ) {
        if ( Trigger.isUpdate ) {
            skedShiftOfferTriggerHandler.onBeforeUpdate(Trigger.New, Trigger.oldMap);
            //CPL2-317: Shift Offer - Date/Time Stamp offer decline
            for ( Shift_Offer__c newRec : Trigger.new ) {
                Shift_Offer__c oldRec = Trigger.oldMap.get(newRec.Id);
                if ( oldRec.Status__c != newRec.Status__c ) {
                    if(newRec.Status__c == 'Declined') newRec.Date_Time_Declined__c = System.now();
                }
            }
        }
    }
    if( Trigger.isAfter) {        
        if ( Trigger.isUpdate) {
            skedShiftOfferTriggerHandler.onAfterUpdate(Trigger.New, Trigger.oldMap);
        }
    }
}