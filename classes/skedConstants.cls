public class skedConstants {
	public static final String REGION_WESTERN_AUSTRALIA = 'Western Australia';
    public static final String STATUS_CURRENT = 'Current';
    public static final String TIMESHEET_STATUS_DRAFT = 'Draft';
    public static final String TIMESHEET_STATUS_PENDING_APPROVAL = 'Pending Approval';
    public static final String TIMESHEET_STATUS_APPROVED = 'Approved';
    public static final String TIMESHEET_STATUS_REJECTED = 'Rejected';
    public static final String JOB_ALLOCATION_DECLINED = 'Declined';
    public static final String JOB_ALLOCATION_DELETED = 'Deleted';
    public static final String JOB_ALLOCATION_MODIFIED = 'Modified';

    public static final string AVAILABILITY_STATUS_APPROVED = 'Approved';
    public static final string AVAILABILITY_STATUS_PENDING = 'Pending';
    public static final string AVAILABILITY_STATUS_DECLINED = 'Declined';

    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    
    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    
    public static final string HOLIDAY_GLOBAL = 'global';

    public static final String CONTACT_CLIENT_RT = 'Client';

    public static final String RES_ACCEPT_OFFER = 'Thanks for accepting this Shift. You are now an allocated Care Provider! Please click Close.';
    public static final String RES_DECLINE_OFFER = 'Thanks for declining the Shift. You will be removed from the Shift Offer list. Please select Close';
    public static final String RES_NOT_AVAI = 'Thank you for checking this Shift. Unfortunately, you have been allocated to another shift, the offer will be removed from your list ';
    public static final String OFFER_NOT_AVAI = 'Thanks for accepting this offer. Unfortunately, it has been allocated or is no longer available. The offer will be removed from your list';

    public static final String OFFER_ACCEPT = 'Accepted';
    public static final String OFFER_DECLINED = 'Declined';
    public static final String OFFER_ALLOCATED = 'Allocated';
    public static final STring OFFER_OFFFERED = 'Offered';
    public static final String OFFER_CANCELLED = 'Cancelled';
}