global virtual class skedCCModels {

    public virtual class resource {
        public string id {get;set;}
        public string name {get;set;}
        public string category {get;set;}
        public string resourceType {get;set;}
        public string photoUrl {get;set;}
        public string regionId {get;set;}
        public Integer certificationMatch {get;set;}
        public Boolean hasRequiredRole {get;set;}
        public string employmentType {get;set;}
        public string timezoneSidId {get;set;}
        public string regionName {get;set;}
        public string userId {get;set;}

        public transient Location geoLocation {get;set;}
        public transient Map<Id, resourceTag> mpTags {get;set;}
        public transient Boolean isFullTime;
        public transient Boolean isAsset;

        public resource() {

        }

        public resource(sked__Resource__c skedResource) {
            this(skedResource, '');
        }

        public resource(sked__Resource__c skedResource, String requiredRole) {
            this.id = skedResource.Id;
            this.name = skedResource.Name;
            this.regionId = skedResource.sked__Primary_Region__c;
            this.photoUrl = skedResource.sked__User__r.SmallPhotoUrl;
            this.resourceType = skedResource.sked__Resource_Type__c;
            this.category = skedResource.sked__Category__c;
            this.geoLocation = skedResource.sked__GeoLocation__c;
            this.mpTags = new Map<Id, resourceTag>();
            this.hasRequiredRole = String.isBlank(requiredRole) ? true : false;
            this.employmentType = skedResource.Employment_Type__c;
            this.isFullTime = skedCCConstants.RESOURCE_FULL_TIME_EMPLOYMENT.equalsIgnoreCase(this.employmentType);
            this.isAsset = skedCCConstants.RESOURCE_TYPE_ASSET.equalsIgnoreCase(this.resourceType);
            if (skedResource.sked__Primary_Region__c != null) {
                this.timezoneSidId = skedResource.sked__Primary_Region__r.sked__Timezone__c;
            } else {
                this.timezoneSidId = '';
            }

            if (skedResource.sked__ResourceTags__r != NULL && !skedResource.sked__ResourceTags__r.isEmpty()) {
                for (sked__Resource_Tag__c skedResourceTag : skedResource.sked__ResourceTags__r) {
                    resourceTag tag = new resourceTag();
                    tag.resourceId = skedResourceTag.sked__Resource__c;
                    tag.tagId = skedResourceTag.sked__Tag__c;
                    tag.expiryDate = skedResourceTag.sked__Expiry_Date__c;
                    mpTags.put(tag.tagId, tag);
                }
            }

            
        }
    }

    public class resourceAllocation extends resource {
        public decimal distance {get;set;}
        public integer travelTime {get;set;}

        public resourceAllocation(sked__Resource__c skedResource) {
            this(skedResource, '');
        }

        public resourceAllocation(sked__Resource__c skedResource, String requiredRole) {
            super(skedResource, requiredRole);
        }
    }

    public class resourceTag {
        public string resourceId {get;set;}
        public string tagId {get;set;}
        public transient DateTime expiryDate {get;set;}
    }

    public class RegionModel {
        public String id;
        public String name;
        public String timeZone;

        public RegionModel(sked__Region__c region) {
            this.id = region.Id;
            this.name = region.Name;
            this.timeZone = region.sked__Timezone__c;
        }
    }

    public class jobInfo implements Comparable {
        public location geoLocation {get;set;}
        public DateTime start {get;set;}
        public DateTime finish {get;set;}
        public DateTime startOfDate {get;set;}
        public String timeZone;

        public Integer compareTo(Object compareTo) {
            jobInfo compareJobInfo = (jobInfo)compareTo;
            if (this.start > compareJobInfo.start) {
                return 1;
            }
            if (this.start == compareJobInfo.start) {
                return 0;
            }
            return -1;
        }
    }

    public class PickListItem {
        public string id    {get;set;}
        public string label {get;set;}
        public string email {get;set;}//Contact
        public string address {get;set;}//Account
        public string city {get;set;}//Account
        public string state {get;set;}//Account
        public string street {get;set;}//Account
        public string postalCode {get;set;}//Account
        public Boolean selected {get;set;}

        public PickListItem(string id, string label){
            this.id     = id;
            this.label  = label;
            this.email  = '';
            this.address = '';
            this.selected = false;
        }

        public PickListItem(sObject obj){
            this.email  = '';
            this.address = '';
            this.selected = false;
            if(obj == null) {
                this.id = '';
                this.label = '';
                return;
            }

            this.id     = (string)obj.get('id');
            this.label  = (string)obj.get('name');

            if(this.id != null && this.id.substring(0,3) == '003'){//Contact
                this.email = (string)obj.get('email');
            }

            if(this.id != null && this.id.substring(0,3) == '001'){//Account
                this.city = (string)obj.get('BillingCity');
                this.state = (string)obj.get('BillingState');
            }
        }
    }

    public virtual class OptionModel {
        public string id;
        public string name;
    }

    

    public class TagModel extends OptionModel{

        public String relatedId;
        public Boolean required;
        public Integer weighting;

        public TagModel(sked__Tag__c tag) {
            this.id = tag.Id;
            this.name = tag.Name;
        }

        public TagModel(Client_Tag__c clientTag) {
            this.relatedId = clientTag.Client__c;
            this.id = clientTag.Tag__c;
            this.name = clientTag.Tag__r.Name;
            this.required = clientTag.Required__c;
            this.weighting = (Integer)(clientTag.Weighting__c);
        }
    }

    public class CordinatorModel extends OptionModel{
        public string key;
        public String photoUrl;

        public CordinatorModel() {

        }

        public CordinatorModel(Contact cont) {
            this.id = cont.Id;
            this.name = cont.Name;
            this.key = cont.AccountID;
            this.photoUrl = cont.PhotoUrl;
        }
    }

    public virtual class Allocation {
        public string id {get;set;}
        public string jobId {get;set;}
        public string resourceId {get;set;}
        public string resourceName {get;set;}
        public decimal distance {get;set;}

        public boolean isAvailable {get;set;}
        public boolean isQualified {get;set;}

        public transient boolean travelFromHome {get;set;}
        public transient boolean travelToHome {get;set;}
        public boolean flightRecommendation;

        public integer travelTimeFrom {get;set;}
        public Geometry startFromLocation {get;set;}

        public jobInfo jobInfo {get;set;}
        public resource resource {get;set;}

        public Allocation() {
            this.isAvailable = true;
            this.isQualified = true;
            this.flightRecommendation = false;
        }
    }

    public class JobLocation {
        public String timeZoneId;
        public String timeZoneName;
        public String regionId;
        public String placeId;
        public String city;
        public String state;
        public String zip;
        public String territory;
        public String fullAddress;
        public decimal latitude {get;set;}
        public decimal longitude {get;set;}

        public JobLocation() {

        }

        public JobLocation(Location loc) {
            this.latitude = loc.getLatitude();
            this.longitude = loc.getLongitude();
        }
    }


    global class Geometry {
        public decimal latitude {get;set;}
        public decimal longitude {get;set;}
        public transient Location geoLoc {get;set;}

        public Geometry(){}

        public Geometry(Location location) {
            setup(location);
        }

        private void setup(Location location) {
            if (location != NULL) {
                this.latitude = location.getLatitude();
                this.longitude = location.getLongitude();
                this.geoLoc = location;
            }
        }
    }
}