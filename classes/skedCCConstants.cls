public class skedCCConstants {
	public static final String STATUS_CURRENT = 'Current';
    
    public static final string AVAILABILITY_STATUS_APPROVED = 'Approved';
    public static final string AVAILABILITY_STATUS_PENDING = 'Pending';
    public static final string AVAILABILITY_STATUS_DECLINED = 'Declined';

    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    
    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_ALLOCATION_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_ALLOCATION_STATUS_CHECKED_IN = 'Checked In';
    public static final string JOB_ALLOCATION_STATUS_ON_SITE = 'On Site'; // On site = Checked In

    public static final string SHIFT_STATUS_PENDING = 'Pending';
    public static final string SHIFT_STATUS_ACTIVE = 'Active';
    public static final string SHIFT_STATUS_COMPLETE = 'Complete';
    public static final string SHIFT_STATUS_FINALISED = 'Finalised';
    
    public static final string HOLIDAY_GLOBAL = 'global';

    //Contact record Type
    public static final string CONTACT_RECORD_TYPE_SUPPORTED_INDIVIDUAL = 'Supported Individual';
    public static final string CONTACT_RECORD_TYPE_RESOURCE = 'Resource';
    public static final string CONTACT_RECORD_TYPE_VOLUNTEER = 'Volunteer';

    public static final String INVALID_ADDRESS_ERROR_MSG = 'Cannot get geolocation from this address!';
    public static final String CONTACT_RECORDTYPE_EMPLOYEE = 'Employee';
    public static final String ADDRESS_TYPE_LOCATION = 'Location';
    public static final String ADDRESS_TYPE_ACCOUNT = 'Account';
    public static String INT_TIME_FORMAT = 'Hmm';
    public static final String UNCATEGORISED_CATEGORY = 'Uncategorised';
    public static final String RESOURCE_FULL_TIME_EMPLOYMENT = 'Full-time';
    public static final string RESOURCE_TYPE_ASSET = 'Asset';

    public static final String DEFAULT_ATTACHMENT_URL = '/servlet/servlet.FileDownload?file=';

    public static String ADDRESS_TYPE{
        get{
            if(skedCCConfigs.getskedConfigsCustomSettings().containsKey('Address_Type')){
                ADDRESS_TYPE     = skedCCConfigs.getskedConfigsCustomSettings().get('Address_Type').Value__c;
            }else ADDRESS_TYPE   = '';
            return ADDRESS_TYPE;
        }
        set;
    }

    public static String ADDRESS_COUNTRY {
        get{
            if(skedCCConfigs.getskedConfigsCustomSettings().containsKey('Address_Country')){
                ADDRESS_COUNTRY     = skedCCConfigs.getskedConfigsCustomSettings().get('Address_Country').Value__c;
            }else ADDRESS_COUNTRY   = '';
            return ADDRESS_COUNTRY;
        }
        set;
    }

    public static String SF_CONTENTVERSION_URL {
        get{
            if(skedCCConfigs.getskedConfigsCustomSettings().containsKey('SF_ContentVersion_URL')){
                SF_CONTENTVERSION_URL     = skedCCConfigs.getskedConfigsCustomSettings().get('SF_ContentVersion_URL').Value__c;
            }else SF_CONTENTVERSION_URL   = '';
            return SF_CONTENTVERSION_URL;
        }
        set;
    }
}