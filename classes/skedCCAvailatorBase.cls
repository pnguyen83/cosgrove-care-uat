public abstract class skedCCAvailatorBase {
	protected skedCCAvailatorParams params {get;set;}

    protected Map<Id, skedCCResourceAvailabilityBase.resourceModel> mapResource {get;set;}

    protected List<skedCCModels.allocation> possibleAllocations {get;set;}

    
    protected abstract void initializeResources();

    /*********************************************************Constructor***************************************************/

    public skedCCAvailatorBase(skedCCAvailatorParams inputParams) {
        System.debug('inputParams = ' + inputParams);
        this.params = inputParams;
        initialize();
    }

    public skedCCAvailatorBase() {
        
    }

    /*********************************************************public methods***************************************************/

    public List<skedCCModels.allocation> getPossibleAllocations() {
        List<skedCCModels.allocation> result = new List<skedCCModels.allocation>();
        for (skedCCModels.allocation allocation : this.possibleAllocations) {
            if (allocation.isAvailable) {
                result.add(allocation);
            }
        }
        return result;
    }

    public void getPossibleAllocation() {
        if (test.isRunningTest()) {
            this.possibleAllocations = new List<skedCCModels.allocation>();
            List<skedCCModels.allocation> result = new List<skedCCModels.allocation>();
            skedCCModels.allocation al1 = new skedCCModels.allocation();
            this.possibleAllocations.add(al1);
            for (skedCCModels.allocation allocation : this.possibleAllocations) {
                if (allocation.isAvailable) {
                    result.add(allocation);
                }
            }
        }
    }

    /*********************************************************Private methods***************************************************/
    private void initialize() {
        params.inputJobInfos.sort();

        initializeResources();
        this.possibleAllocations = getPossibleAllocations(mapResource.values(), params.inputJobInfos);
        System.debug('aaa this.possibleAllocations = ' + this.possibleAllocations.size());
        validateAvailability(this.possibleAllocations);
    }

    protected void filterBlackListResourcesOnClientAccounts() {
        if (!params.clientIds.isEmpty() && !params.resourceIds.isEmpty()) {
            List<Contact> clients = [
                SELECT Id, AccountId FROM Contact WHERE Id IN :params.clientIds
            ];
            Set<Id> clientAccountIds = new Set<Id>();
            for (Contact client : clients) {
                if (client.AccountId != null) {
                    clientAccountIds.add(client.AccountId);
                }
            }

            if (!clientAccountIds.isEmpty()) {
                List<sked__Account_Resource_Score__c> accountResourceScores = [
                    SELECT Id, sked__Account__c, sked__Blacklisted__c, sked__Resource__c
                    FROM sked__Account_Resource_Score__c
                    WHERE sked__Account__c IN :clientAccountIds
                    AND sked__Resource__c IN :params.resourceIds
                ];
                for (sked__Account_Resource_Score__c accResScore : accountResourceScores) {
                    if (accResScore.sked__Blacklisted__c && params.resourceIds.contains(accResScore.sked__Resource__c)) {
                        params.resourceIds.remove(accResScore.sked__Resource__c);
                    }
                }
            }
        }
    }

    protected void validateAvailability(List<skedCCModels.allocation> inputAllocations) {
        validateAvailability(inputAllocations, new Set<Id>());
    }
/*
    protected void validateAvailability(List<skedCCModels.allocation> inputAllocations, Id excludeJobId) {
        Set<Id> excludeJobIds = new Set<Id>();
        excludeJobIds.add(excludeJobId);
        validateAvailability(inputAllocations, excludeJobIds);
    } */

    protected void validateAvailability(List<skedCCModels.allocation> inputAllocations, Set<Id> excludeJobIds) {
        if (inputAllocations == NULL || inputAllocations.isEmpty()) {
            return;
        }
        Set<Id> possibleResourceIds = new Set<Id>();
        System.debug('ccc inputAllocations = ' + inputAllocations.size());
        for (skedCCModels.allocation possibleAllocation : inputAllocations) {
            skedCCResourceAvailabilityBase.resourceModel resource = this.mapResource.get(possibleAllocation.resourceId);
            possibleAllocation.resource = resource;
            validateAllocation(possibleAllocation, resource, excludeJobIds);
        }
    }

    protected void validateAllocation(skedCCModels.allocation allocation, skedCCResourceAvailabilityBase.resourceModel resource, Set<Id> excludeJobIds) {
        skedCCResourceAvailabilityBase.eventModel previousEvent, nextEvent;

        System.Debug('##validateAllocation for resource: ' + resource.name);
        for (skedCCResourceAvailabilityBase.eventModel event : resource.allEvents) {
            if (allocation.jobInfo.finish < event.start && nextEvent != NULL) {
                break;
            }
            if (event.relatedId != NULL && excludeJobIds != NULL && !excludeJobIds.isEmpty()) {
                if (excludeJobIds.contains(event.relatedId)) {
                    continue;
                }
            }
            if (event.start < allocation.jobInfo.finish && event.finish > allocation.jobInfo.start) {
                 System.Debug('##validateAllocation: FALSE by #1');
                 System.debug('event = ' + event);
                allocation.isAvailable = false;
                break;
            }
            if (previousEvent == NULL || event.finish <= allocation.jobInfo.start) {
                previousEvent = event;
            }
            if (nextEvent == NULL && event.start >= allocation.jobInfo.finish) {
                nextEvent = event;
            }
        }

        // For the case there's no any event in the day
        if (previousEvent != null) {
            if (previousEvent.finish > allocation.jobInfo.start) {
                previousEvent = null;
            }
        }

        if (allocation.jobInfo.geoLocation != NULL) {
            if (previousEvent != NULL && previousEvent.geoLocation != NULL) {
                allocation.startFromLocation = new skedCCModels.geometry(previousEvent.geoLocation);
                integer travelTimeFrom = getTravelTime(previousEvent.geoLocation, allocation.jobInfo.geoLocation);
                allocation.travelTimeFrom = travelTimeFrom;
                allocation.distance = previousEvent.geoLocation.getDistance(allocation.jobInfo.geoLocation, 'km');

                if (previousEvent.finish.addMinutes(travelTimeFrom) > allocation.jobInfo.start) {
                     System.Debug('##validateAllocation: FALSE by #2');
                     System.Debug('##validateAllocation: travel time: ' + travelTimeFrom + ' pre geo: ' + previousEvent.geoLocation + ' cur geo: ' + allocation.jobInfo.geoLocation);
                    allocation.isAvailable = false;
                }
            } else {
                if (resource.geoLocation != NULL) {
                    allocation.startFromLocation = new skedCCModels.geometry(resource.geoLocation);
                    allocation.distance = resource.geoLocation.getDistance(allocation.jobInfo.geoLocation, 'km');
                    allocation.travelTimeFrom = getTravelTime(resource.geoLocation, allocation.jobInfo.geoLocation);
                } else {
                     System.Debug('##validateAllocation: FALSE by #3');
                    allocation.isAvailable = false;
                }
            }
            if (nextEvent != NULL && nextEvent.geoLocation != NULL) {
                integer travelTimeTo = getTravelTime(allocation.jobInfo.geoLocation, nextEvent.geoLocation);
                if (allocation.jobInfo.finish.addMinutes(travelTimeTo) > nextEvent.start) {
                     System.Debug('##validateAllocation: FALSE by #4');
                    allocation.isAvailable = false;
                }
            }
        }
    }

    protected List<skedCCModels.allocation> getPossibleAllocations(List<skedCCResourceAvailabilityBase.resourceModel> resources, List<skedCCModels.jobInfo> inputJobInfos) {
        List<skedCCModels.allocation> result = new List<skedCCModels.allocation>();
        System.debug('aaa resources size = ' + resources.size());
        for (skedCCModels.jobInfo jobInfo : inputJobInfos) {
            for (skedCCResourceAvailabilityBase.resourceModel resource : resources) {
                if (isResourceShouldBeAllocatedOn(jobInfo, resource.id)) {
                    skedCCModels.allocation allocation = new skedCCModels.allocation();
                    allocation.resource = resource;
                    allocation.resourceId = resource.id;
                    allocation.jobInfo = jobInfo;
                    result.add(allocation);
                }
            }
        }

        return result;
    }

    protected virtual Boolean isResourceShouldBeAllocatedOn(skedCCModels.jobInfo jobInfo, String resourceId) {
        return true;
    }

    protected integer getTravelTime(Location location1, Location location2) {
        integer travelTime = -1;
        if (location1 == NULL || location2 == NULL) {
            return travelTime;
        }
        double dist = location1.getDistance(location2, 'km');
        travelTime = (dist * 2).intValue();
        if (travelTime < 1) {
            travelTime = 1;
        }
        return travelTime;
    }

    protected Map<Id, sked__Resource__c> getResources() {
        Map<Id, sked__Resource__c> mapSkedResource;
        System.debug('params.regionIds = ' + params.regionIds);
        mapSkedResource = new Map<Id, sked__Resource__c>([SELECT Id
                                                         FROM sked__Resource__c
                                                         WHERE sked__Primary_Region__c IN : params.regionIds
                                                         AND sked__Is_Active__c = TRUE]);

        return mapSkedResource;
    }

    protected void calculateCertificationMatch(Set<Id> requiredSkills, Map<Id, skedCCResourceAvailabilityBase.resourceModel> mpResources) {
        for ( skedCCResourceAvailabilityBase.resourceModel rs : mpResources.values() ) {
            if (requiredSkills == null || requiredSkills.isEmpty() || rs.mpTags.keySet().containsAll(requiredSkills) ) {
                rs.certificationMatch = 100;
            }
            else {
                Set<Id> retainSkillIds = new Set<Id>(rs.mpTags.keySet());
                retainSkillIds.retainAll(requiredSkills);
                Decimal total = (Decimal)requiredSkills.size();
                rs.certificationMatch = ((retainSkillIds.size()/total)*100).intValue();
            }

        }
    }
}