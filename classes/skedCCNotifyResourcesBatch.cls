global class skedCCNotifyResourcesBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    List<sked__Job_Allocation__c> allocations;

    global skedCCNotifyResourcesBatch(List<sked__Job_Allocation__c> inputAllocations) {
        allocations = inputAllocations;
    }

    global List<sObject> start(Database.BatchableContext BC) {        
        return allocations;
    }

    global void execute(Database.BatchableContext BC, List<sked__Job_Allocation__c> scope) {
        Set<Id> allocationIds = new Set<Id>();

        for (sked__Job_Allocation__c allocation : scope) {
            allocationIds.add(allocation.Id);
        }

        if (!allocationIds.isEmpty()) {
            skedCCSkeduloApiManager.notifyResources(allocationIds);
        }        
    }

    global void finish(Database.BatchableContext BC) {
        
    }   

}