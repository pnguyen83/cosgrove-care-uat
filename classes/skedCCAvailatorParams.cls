global class skedCCAvailatorParams {
    public string timezoneSidId {get;set;}
    public boolean respectResourceTimezone {get;set;}
    public Set<Id> regionIds {get;set;}
    public Set<Id> resourceIds {get;set;}
    public Set<Id> jobTagIds {get;set;}
    public Set<Id> Roles {get;set;}
    public Set<Id> clientIds;
    public List<skedCCModels.jobInfo> inputJobInfos {get;set;}
    public Boolean includeResourceInfo{get;set;}
    public Integer intStartTime{get;set;}
    public Integer intEndTime{get;set;}

    public skedCCAvailatorParams() {
        this.timezoneSidId = UserInfo.getTimeZone().getID();
        this.respectResourceTimezone = false;
        this.regionIds = new Set<Id>();
        this.resourceIds = new Set<Id>();
        this.inputJobInfos = new List<skedCCModels.jobInfo>();
        this.includeResourceInfo = false;
        this.jobTagIds = new Set<Id>();
        this.Roles = new Set<Id>();
        this.clientIds = new Set<Id>();
    }
}