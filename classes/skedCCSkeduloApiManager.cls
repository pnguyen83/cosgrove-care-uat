global class skedCCSkeduloApiManager {
    public static final String SKEDULO_END_POINT    = 'callout:Skedulo_API/notifications/';

    @future(callout = true)
    global static void sendNotification(Set<Id> jobAllocationIds) {
        notifyResources(jobAllocationIds);
    }

    global static void notifyResources(Set<Id> jobAllocationIds) {
        List<sked__Job_Allocation__c> jobAllocs = [
            SELECT Id, sked__Job__c, sked__Resource__c, Skedulo_API_Error_Message__c
            FROM sked__Job_Allocation__c
            WHERE Id IN :jobAllocationIds
            AND sked__Status__c NOT IN (:skedCCConstants.JOB_ALLOCATION_STATUS_DELETED, :skedCCConstants.JOB_ALLOCATION_STATUS_DECLINED)
        ];

        for (sked__Job_Allocation__c jobAlloc : jobAllocs) {
            System.debug('1111 jobAlloc = ' + jobAlloc);
            ApiResponse result = notify(jobAlloc);
        }

        update jobAllocs;
    }

    private static ApiResponse notify(sked__Job_Allocation__c jobAlloc) {
        ApiResponse result = new ApiResponse();
        Http http      = new Http();
        HttpRequest req    = new HttpRequest();
        HttpResponse res  = new HttpResponse();
        NotificationRequest request = new NotificationRequest();
        request.jobId = jobAlloc.sked__Job__c;
        request.resourceId = jobAlloc.sked__Resource__c;

        //Set end point to Authenciate
        String endPoint = SKEDULO_END_POINT + 'notify';
        String jsonReq = JSON.serialize(request);

        System.debug('EndPoint = ' + EndPoint);
        req.setEndpoint( EndPoint );
        req.setMethod('POST');
        req.setTimeout(10000);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(jsonReq);
        string jsonResponse;

        try {
            if (test.isRunningTest()) {
                jsonResponse = '{"result":{"jobId":"a195E000001g061QAA","results":[{"resourceId":"a1H6E000000UrRzUAK","protocol":"push","error":{"errorType":"recipient_error","message":"No device found for resource a1H6E000000UrRzUAK","errorKey":"no_device"}}]}}';
            }
            else {
                res = http.send(req);
                jsonResponse = res.getBody();
            }
            System.debug(jsonResponse);
            NotificationResponseRoot respondRoot = (NotificationResponseRoot)JSON.deserialize(jsonResponse, NotificationResponseRoot.class);
            Map<Id, NotificationResponseResult> mpResults = new Map<Id, NotificationResponseResult>();
            if (respondRoot.result != null && respondRoot.result.results != null) {
                result.success = true;
            }
        }
        catch (JSONException jsonEx) {
            result.success = FALSE;
            result.errorMessage = 'An error happens in Skedulo Server. Please contact administrator.';
            result.errorMessage += (' Json Response: ' + jsonResponse);
        }
        
        return result;
    }

	global class ApiResponse {
		public boolean success {get;set;}
		public string errorMessage {get;set;}
		public string errorCode {get;set;}
	}

    global class NotificationRequest {
        public String jobId {get;set;}
        public String resourceId {get;set;}
    }

    global class NotificationResponseRoot {
        public NotificationResponse result;
    }

    global class NotificationResponse {
        public String jobId {get;set;}
        public List<NotificationResponseResult> results;
    }

    global class NotificationResponseResult {
        public String resourceId {get;set;}
        public String protocol {get;set;}
        public NotificationError error {get;set;}
    }

    global class NotificationError {
        public String errorType {get;set;}
        public String message {get;set;}
    }
}