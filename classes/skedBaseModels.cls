global class skedBaseModels {

    public virtual class resource {
        public string id {get;set;}
        public string name {get;set;}
        public string category {get;set;}
        public string photoUrl {get;set;}
        public string regionId {get;set;}
        public string regionName {get;set;}
        public string userId {get;set;}
        public string timezoneSidId {get;set;}
        
        public resource(sked__Resource__c skedResource) {
            this.id = skedResource.Id;
            this.name = skedResource.Name;
            this.category = skedResource.sked__Category__c;
            this.regionId = skedResource.sked__Primary_Region__c;
            this.regionName = skedResource.sked__Primary_Region__r.Name;
            this.timezoneSidId = skedResource.sked__Primary_Region__r.sked__Timezone__c;
            if (skedResource.sked__User__c != NULL) {
                this.userId = skedResource.sked__User__c;
                this.photoUrl = skedResource.sked__User__r.SmallPhotoUrl;
            }
        }
    }
    
}