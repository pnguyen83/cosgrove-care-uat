public class skedJobAllocationHandler {
    public void afterInsert(List<sked__Job_Allocation__c> lstNews) {
        dispatchJobs(lstNews, null);    
    }

    public void afterUpdate(List<sked__Job_Allocation__c> lstNews, Map<Id, sked__Job_Allocation__c> mapOlds) {
        dispatchJobs(lstNews, mapOlds);
    }

    private void dispatchJobs(List<sked__Job_Allocation__c> lstNews, Map<Id, sked__Job_Allocation__c> mapOlds) {
        List<sked__Job_Allocation__c> lstDispatchJAs = new List<sked__Job_Allocation__c>();
        for ( sked__Job_Allocation__c newJobAll : lstNews ) {
            if (newJobAll.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED ||
                    newJobAll.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_DISPATCHED) {
                if (mapOlds != null) {
                    sked__Job_Allocation__c oldJobAll = mapOlds.get(newJobAll.Id);

                    if ( oldJobAll.sked__Status__c != newJobAll.sked__Status__c && newJobAll.sked__Status__c != skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED) {
                        lstDispatchJAs.add(newJobAll);
                    }   
                }
                else {
                    lstDispatchJAs.add(newJobAll);
                }
            }
            
        }

        if (!lstDispatchJAs.isEmpty()) {
            System.debug('1 lstDispatchJAs = ' + lstDispatchJAs);
            dispatchJobs(lstDispatchJAs);   
        }
    }

    private void dispatchJobs(List<sked__Job_Allocation__c> newJoblstDispatchJAs) {
        //skedCCNotifyResourceSchedule sch = new skedCCNotifyResourceSchedule(newJoblstDispatchJAs);
        //String schStr = '0 0 * * * ?';
        //System.schedule('run schedule job', schStr, sch);
        //sch.execute(null);
        Set<Id> setJaIDs = new Set<Id>();
        for (sked__Job_Allocation__c jobAlloc : newJoblstDispatchJAs) {
            if ( jobAlloc.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED ||
                    jobAlloc.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_DISPATCHED)
                setJaIDs.add(jobAlloc.id);
        }
        
        if (!setJaIDs.isEmpty()) {
            System.debug('2 setJaIDs = ' + setJaIDs);
            skedCCSkeduloApiManager.sendNotification(setJaIDs);
        } 
    }
}