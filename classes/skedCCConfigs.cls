public class skedCCConfigs {

    private static Map<String, skedConfigs__c> skedConfigsInstance = null;

    /**
    *@description Singleton to get skedConfig custom setting
    *
    */
    public static Map<String, skedConfigs__c> getskedConfigsCustomSettings() {
        if(skedConfigsInstance == null) {
            skedConfigsInstance = skedConfigs__c.getAll();
        }
        return skedConfigsInstance;
    }

	public static String JOB_URL{//
        get{
            if(getskedConfigsCustomSettings().containsKey('Global_JobURL')){
                JOB_URL     = getskedConfigsCustomSettings().get('Global_JobURL').Value__c;
            }else JOB_URL   = '/';
            return JOB_URL;
        }
        set;
    }

    public static Integer DEFAULT_JOB_DURATION {//
        get{
            if(getskedConfigsCustomSettings().containsKey('Global_Default_Job_Duration')){
                DEFAULT_JOB_DURATION     = Integer.valueOf(getskedConfigsCustomSettings().get('Global_Default_Job_Duration').Value__c);
            }else DEFAULT_JOB_DURATION   = 60;
            return DEFAULT_JOB_DURATION;
        }
        set;
    }

    public static String DEFAULT_DELIVERY_METHOD{//
        get{
            if(getskedConfigsCustomSettings().containsKey('Global_DefaultDeliveryMethod')){
                DEFAULT_DELIVERY_METHOD     = getskedConfigsCustomSettings().get('Global_DefaultDeliveryMethod').Value__c;
            }else DEFAULT_DELIVERY_METHOD   = '';
            return DEFAULT_DELIVERY_METHOD;
        }
        set;
    }

    public static String SKEDULO_API_TOKEN{
        get{
            if(getskedConfigsCustomSettings().containsKey('Global_SkeduloAPIToken')){
                SKEDULO_API_TOKEN     = getskedConfigsCustomSettings().get('Global_SkeduloAPIToken').Value__c;
            }else SKEDULO_API_TOKEN   = '';
            return SKEDULO_API_TOKEN;
        }
        set;
    }
}