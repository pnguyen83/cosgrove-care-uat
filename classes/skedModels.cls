global class skedModels {
    
    public virtual class resource extends skedBaseModels.resource {
        public string subCategory {get;set;}
        public string employmentType {get;set;}
        
        public resource(sked__Resource__c skedResource) {
         	super(skedResource);
            this.subCategory = 'Sub category';
            this.employmentType = skedResource.Employment_Type__c;
        }
    }

}