public class skedCCAvailator extends skedCCAvailatorBase{
	private skedCCResourceAvailabilityBase resourceAvailability {
        get {
            if (resourceAvailability == NULL) {
                resourceAvailability = new skedCCResourceAvailability();
            }
            return resourceAvailability;
        }
        set;
    }

    public skedCCAvailator(skedCCAvailatorParams inputParams) {
        super(inputParams);
    }

    public skedCCAvailator() {
        
    }

    protected override void initializeResources() {
        DateTime firstJobStart, lastJobFinish;
        for (skedCCModels.jobInfo jobInfo : params.inputJobInfos) {
            if (firstJobStart == NULL || firstJobStart > jobInfo.start) {
                firstJobStart = jobInfo.startOfDate;
            }
            if (lastJobFinish == NULL || lastJobFinish < jobInfo.finish) {
                lastJobFinish = jobInfo.finish;
            }
        }

        if (params.resourceIds.isEmpty() && !params.regionIds.isEmpty()) {
            Map<Id, sked__Resource__c> mapSkedResource = getResources();
            params.resourceIds = mapSkedResource.keySet();
        }
        
        String requiredRole = '';
        if (params.Roles != null && !params.Roles.isEmpty()) {
            requiredRole = (new list<Id>(params.Roles) )[0];
        }

        filterBlackListResourcesOnClientAccounts();

        this.mapResource = this.resourceAvailability.initializeResourceList(params.resourceIds, firstJobStart, lastJobFinish, params.timezoneSidId, requiredRole);
        System.debug('aaa 1 this.mapResource = ' + this.mapResource.size());
        System.debug('aaa 1 this.params.jobTagIds = ' + this.params.jobTagIds);
        calculateCertificationMatch(this.params.jobTagIds, mapResource);
        System.debug('aaa 2 this.mapResource = ' + this.mapResource.size());
        removeNonQualifiedResources(mapResource);
        System.debug('aaa 3 this.mapResource = ' + this.mapResource.size());
    }

    public List<skedCCModels.resourceAllocation> getAvailableResources() {
        skedCCModels.jobInfo firstJobInfo = this.params.inputJobInfos.get(0);
        Set<Id> resourceIds = new Set<Id>();
        Map<String, skedCCModels.allocation> resourceIdToAllocationMap = new Map<String, skedCCModels.allocation>();
        List<skedCCModels.resourceAllocation> result = new List<skedCCModels.resourceAllocation>();
        
        for (skedCCModels.allocation allocation : this.possibleAllocations) {
            if (allocation.isAvailable) {
                resourceIds.add(allocation.resourceId);
                resourceIdToAllocationMap.put(allocation.resourceId, allocation);
            }
        }
        
        List<sked__Resource__c> skedResources = [SELECT Id, Name, sked__User__r.SmallPhotoUrl, Employment_Type__c, sked__User__c, sked__Resource_Type__c, sked__Category__c,
                                                 sked__GeoLocation__c, sked__GeoLocation__Latitude__s, sked__GeoLocation__Longitude__s, sked__Home_Address__c, 
                                                 sked__Primary_Region__c, sked__Primary_Region__r.sked__Timezone__c
                                                 FROM sked__Resource__c
                                                 WHERE Id IN :resourceIds ORDER By Name];
        for (sked__Resource__c skedResource : skedResources) {
            skedCCModels.resourceAllocation resourceAll = new skedCCModels.resourceAllocation(skedResource);
            if (resourceIdToAllocationMap.containsKey(skedResource.Id)) {
                skedCCModels.allocation allocation = resourceIdToAllocationMap.get(skedResource.Id);
                resourceAll.distance = allocation.distance;
                resourceAll.travelTime = allocation.travelTimeFrom;
            }
            result.add(resourceAll);
        }

        return result;
    }

    private void removeNonQualifiedResources(Map<Id, skedCCResourceAvailabilityBase.resourceModel> mpResources) {
        removeResourcesByRolesAndTags(mpResources);
        
    }

    private void removeResourcesByRolesAndTags(Map<Id, skedCCResourceAvailabilityBase.resourceModel> mpResources) {
        Set<Id> removeResourceIds = new Set<Id>();
        skedCCResourceAvailabilityBase.resourceModel resourceMod;

        for (Id resourceId : mpResources.keySet()) {
            resourceMod = mpResources.get(resourceId);
            if ((resourceMod.certificationMatch < 100 )) {
                // Remove non qualified resources who don't have all required tags, or resources type person don't have required role
                removeResourceIds.add(resourceId);
            }
        }

        for (Id removedId : removeResourceIds) {
            mpResources.remove(removedId);
        }
    }
}