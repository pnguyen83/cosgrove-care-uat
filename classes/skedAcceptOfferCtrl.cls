public class skedAcceptOfferCtrl extends skedCCModels{
	@remoteAction
	public static skedResponse fetchShiftOffers(String resourceId) {
		System.debug('resourceId = ' + resourceId);
		skedResponse response = new skedResponse();
		Map<String, JobOffer> mapJobOffer = new Map<String, JobOffer>();
		SavePoint sp = Database.setSavePoint();
		try {
			sked__Resource__c res = [SELECT Id, Name, sked__Primary_Region__c, sked__Primary_Region__r.sked__Timezone__c 
									FROM sked__Resource__c 
									WHERE Id = :resourceId ORDER BY Name];
			
			if (res != null) {
				List<Offer> lstOffers = new List<Offer>();
				List<Shift_Offer__c> lstCancelOffers = new List<Shift_Offer__c>();
				
				String timezone = res.sked__Primary_Region__r.sked__Timezone__c;
				
				DateTime currentDate = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
				currentDate	= skedDateTimeUtils.switchTimezone(currentDate, timezone, UserInfo.getTimeZone().getId());
				for (Shift_Offer__c shiftOffer : [SELECT Id, Status__c, Resource__c, Job__c, Job__r.sked__Contact__c, Job__r.sked__Contact__r.Name, 
													Job__r.sked__Type__c, Job__r.sked__Start__c, Job__r.sked__Address__c, Job__r.sked__Finish__c, 
													Job__r.sked__Description__c, Job__r.sked_No_of_Required_Resource__c 
												FROM Shift_Offer__c WHERE Resource__c = :resourceId
												AND Status__c = :skedConstants.OFFER_OFFFERED
												AND Job__r.sked__Start__c >= :currentDate
												ORDER BY Job__r.sked__Start__c]) {
					skedResponse checkResponse = getMapAvaiResourceForOffer(shiftOffer.Job__c);
					
					Set<String> setResIds = (Set<String>)checkResponse.data;
					JobOffer jOffer = mapJobOffer.get(shiftOffer.Job__c);
					if (jOffer == null) {
						jOffer = new JobOffer(shiftOffer.Job__c, shiftOffer.Job__r.sked_No_of_Required_Resource__c);
					}

					if (setResIds != null && setResIds.contains(shiftOffer.Resource__c)) {
						Offer offerModel = new Offer(shiftOffer);
						jOffer.mapOffers.put(shiftOffer.id, offerModel);	
					}
					else {
						shiftOffer.Status__c = skedConstants.OFFER_CANCELLED;
						jOffer.mapCancelOffers.put(shiftOffer.id, shiftOffer);
					}
					mapJobOffer.put(shiftOffer.Job__c, jOffer);
				}	
				
				for (sked__Job__c job : [SELECT Id, sked_No_of_Required_Resource__c, 
												(SELECT Id 
													FROM sked__Job_Allocations__r 
													WHERE sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED
													AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED)
											FROM sked__Job__c
											WHERE Id IN :mapJobOffer.keySet()]) {
					if (mapJobOffer.containsKey(job.id)) {
						JobOffer jOffer = mapJobOffer.get(job.id);
						if (!job.sked__Job_Allocations__r.isEmpty() && job.sked__Job_Allocations__r.size() == job.sked_No_of_Required_Resource__c) {
							for (Offer ofModel : jOffer.mapOffers.values()) {
								Shift_Offer__c sOffer = new Shift_Offer__c(id = ofModel.id, Status__c = skedConstants.OFFER_CANCELLED);
								jOffer.mapCancelOffers.put(sOffer.id, sOffer);
							}

							jOffer.mapOffers.clear();
						}
						mapJobOffer.put(job.id, jOffer);
					}
				}

				for (JobOffer jOffer : mapJobOffer.values()) {
					if (!jOffer.mapOffers.isEmpty()) {
						lstOffers.addAll(jOffer.mapOffers.values());
					}
					if (!jOffer.mapCancelOffers.isEmpty()) {
						lstCancelOffers.addAll(jOffer.mapCancelOffers.values());
					}
				}
				System.debug('lstCancelOffers = ' + lstCancelOffers);
				response.success = true;
				response.data = lstOffers;

				if (!lstCancelOffers.isEmpty()) {
					update lstCancelOffers;
				}
			}
			
		}
		catch(Exception ex) {
			System.debug('Error: ' + ex.getMessage() + ', at line: ' + ex.getStackTraceString());
			response.getErrorMessage(ex);
			Database.rollback(sp);
		}
		

		return response;
	}

	@remoteAction 
	public static skedResponse updateShiftOfferStatus(String offerId, String status) {
		skedResponse response = new skedResponse();
		SavePoint sp = Database.setSavePoint();

		try {
			Shift_Offer__c offer = [SELECT Id, Job__c, Resource__c, Status__c FROM Shift_Offer__c WHERE Id = :offerId];
			String jobId = offer.job__c;
			String resId = offer.Resource__c;

			if (status.equalsIgnoreCase(skedConstants.OFFER_DECLINED)) {
				response.message = skedConstants.RES_DECLINE_OFFER;
                response.success = true;
				offer.Status__c = status;
				update offer;
			}
			else if (status.equalsIgnoreCase(offer.Status__c) || (offer.Status__c == skedConstants.OFFER_CANCELLED)) {
				response.success = true;
				response.message = skedConstants.OFFER_NOT_AVAI;
			}
			else {
				skedResponse checkResponse = getMapAvaiResourceForOffer(offer.Job__c);
				
				if (checkResponse.success) {
					response.success = true;

					Set<String> setResIds = (Set<String>)checkResponse.data;

					if (setResIds == null || setResIds.isEmpty()) {
						response.message = skedConstants.RES_NOT_AVAI;
						offer.Status__c = skedConstants.OFFER_CANCELLED;
					}
					else {
						if (setResIds.contains(resId)) {
							offer.Status__c = status;
							if (status.equalsIgnoreCase(skedConstants.OFFER_ACCEPT)) {
								response.message = skedConstants.RES_ACCEPT_OFFER;
							}
						}
						else {
							offer.Status__c = skedConstants.OFFER_CANCELLED;
							response.message = skedConstants.RES_NOT_AVAI;
						}

						update offer;
					}
				}
				else {
					response.errorMessage = checkResponse.errorMessage;
					response.devMessage = checkResponse.devMessage;
					System.debug('checkResponse = ' + checkResponse);
				}
			}
		}
		catch (Exception ex) {
			response.getErrorMessage(ex);
            System.debug('Error: ' + ex.getMessage());
            System.debug('Error: ' + ex.getStackTraceString());
			Database.rollback(sp);
		}

		return response;
	}

	@remoteAction
	public static skedResponse fetchResource(String userId) {
		skedResponse response = new skedResponse();

		try {
			List<sked__Resource__c> lstRes = [SELECT Id, Name, sked__Primary_Region__c, sked__Primary_Region__r.sked__Timezone__c
										FROM sked__Resource__c WHERE sked__User__c = : userId ORDER BY Name];
			if (lstRes != null && !lstRes.isEmpty()) {
				Resource resModel = new Resource(lstRes.get(0));
				response.success = true;
				response.data = resModel;
			}
		}
		catch (Exception ex) {
			response.getErrorMessage(ex);
		}

		return response;	
	}

//================================================================Private function================================================//
	private static skedResponse getMapAvaiResourceForOffer(String jobId) {
		Set<String> setResIds = new Set<String>();

		skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
		skedResponse response = handler.checkAvaiResourceForOffer(jobId, false);

		if (response.success) {
			List<resourceAllocation> lstRes = (List<resourceAllocation>)response.data;
			for (resourceAllocation res: lstRes) {
				setResIds.add(res.id);
			}
			response.data = setResIds;
		}

		return response;
	}

//====================================================================Child Classes====================================================//
	public class Offer {
		public String id;
		public String status;
		public String resourceId;
		public Item contact;
		public String jobType;
		public DateTime jobStart;
		public DateTime jobFinish;
		public String jobAddress;
		public String jobDesc;

		public Offer(Shift_Offer__c offer) {
			this.id = offer.id;
			this.status = offer.Status__c;
			this.resourceId = offer.Resource__c;
			this.contact = new Item(offer.Job__r.sked__Contact__c, offer.Job__r.sked__Contact__r.Name);
			this.jobType = offer.Job__r.sked__Type__c;
			this.jobStart = offer.Job__r.sked__Start__c;
			this.jobFinish = offer.Job__r.sked__Finish__c;
			this.jobAddress = offer.Job__r.sked__Address__c;
			this.jobDesc = offer.Job__r.sked__Description__c;
		}
	}

	public class Item {
		public String id;
		public String name;

		public Item(String id, String name) {
			this.id = id;
			this.name = name;
		}
	}

	public class Resource {
		public String id;
		public String name;
		public String timezone;

		public Resource(sked__Resource__c res) {
			this.id = res.id;
			this.name = res.name;
			this.timezone = res.sked__Primary_Region__r.sked__Timezone__c;
		}
	}

	public class JobOffer {
		public string id;
		public Decimal requiredOffers;
		public Map<String, Offer> mapOffers;
		public Map<String, Shift_Offer__c> mapCancelOffers;

		public JobOffer(String id, Decimal nRequired) {
			this.requiredOffers = nRequired;
			this.id = id;
			this.mapOffers = new Map<String, Offer>();
			this.mapCancelOffers = new Map<String, Shift_Offer__c>(); 
		}
	}
}